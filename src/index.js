import path from '../node_modules/path-browserify';
import JSZip from '../node_modules/jszip';

import EPub from './epub';

console.log(epub);

const root = document.querySelector('section.root');
const fileName = document.querySelector('.file-name');

const handleFile = e => {
  const file = e.currentTarget.files.length ? e.currentTarget.files[0] : null;
  if (file) {
    fileName.textContent = file.name;
    const fr = new FileReader();
    fr.onload = handleZippedFile;
    fr.readAsArrayBuffer(file);
  }
}

const handleZippedFile = async e => {

  const zip = await JSZip.loadAsync(e.currentTarget.result)
  const filtered = zip.filter(relativePath => relativePath.endsWith('.opf'));
  const opfFile = filtered.length ? filtered[0] : null;
  if (!opfFile) return;

  const rawOPF = await opfFile.async('text');
  if (!rawOPF) return;

  const baseContentDir = path.dirname(opfFile.name);

  const parser = new DOMParser();
  const opf = parser.parseFromString(rawOPF, 'application/xml');

  const imageRefs = [...opf.querySelectorAll('item[media-type^="image/"]')].map(r => `${baseContentDir}/${r.getAttribute('href')}`);

  if (!imageRefs.length) return;

  [...root.children].forEach(c => c.remove());
  imageRefs.forEach(ref => {
    zip.file(ref).async('blob').then(blob => {
      const url = URL.createObjectURL(blob);
      if (url) insertImageAndInput(url)
    })
  })
}

const insertImageAndInput = imgURL => {
  const section = document.createElement('section');
  const imgDiv = document.createElement('div');

  const img = document.createElement('img');
  imgDiv.append(img);
  img.setAttribute('src', imgURL);

  const inputDiv = imgDiv.cloneNode();
  const input = document.createElement('textarea');
  inputDiv.append(input);
  input.setAttribute('rows', 5);

  section.append(imgDiv, inputDiv);

  root.append(section);
}

const fileUpload = document.querySelector('.file-upload');
fileUpload.addEventListener('change', handleFile);
